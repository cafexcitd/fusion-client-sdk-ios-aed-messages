Note that this project source code is provided without the Fusion Client iOS 
SDK Framework.  The framework is excluded in order to deliver the sample code
in a reasonably sized bundle - the framework adds ~0.5GB to the file size.

Once the project is downloaded, add the Fusion Client iOS SDK framework by 
dropping it in the Xcode 'Frameworks' collection located in the project
explorer.
