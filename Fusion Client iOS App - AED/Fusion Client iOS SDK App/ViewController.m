//
//  ViewController.m
//  Fusion Client iOS SDK App
//
//

#import "ViewController.h"
#import "ConfigViewController.h"

@interface ViewController ()

@end

@implementation ViewController

static NSString *URL_KEY = @"url";

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated
{
    // connect to the topic specified by the other controller
    NSString *topicName = [ConfigViewController getTopic];
    ACBUC *acbuc = [ConfigViewController getACBUC];
    self.topic = [acbuc.aed createTopic:topicName];
    self.topic.delegate = self;
    
    // now connect to the topic
    [self.topic connect];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

////////////////////////////////////////////
//
// UI Methods

-(IBAction)updateURI:(id)sender
{
    // just submit - we'll update the UI when we've discovered the update
    // was successfully completed
    [self.topic submitDataWithKey:URL_KEY value:self.uri.text];
}

-(IBAction)sendMessage:(id)sender
{
    [self.topic sendAedMessage:self.message.text];
}

-(void) setWebViewAddress:(NSString *)address
{
    NSURL *url = [NSURL URLWithString:address];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

-(IBAction)userDoneEnteringText:(id)sender
{
    [sender resignFirstResponder];
}

-(void)logMessage:(NSString*)message
{
    self.log.text = [NSString stringWithFormat:@"%@\n%@", message, self.log.text];
    NSLog(@":::%@", message);
}

////////////////////////////////////////////
//
// ACBTopicDelegate methods

- (void)topic:(ACBTopic *)topic didConnectWithData:(NSDictionary *)data
{
    NSArray *topicData = [data objectForKey:@"data"];
    for (NSDictionary *datum in topicData) {
        NSString *currentKey = (NSString *)[datum objectForKey:@"key"];
        
        // if the key for the current item of data matches something our app
        // should respond to, then extract the value and process it!
        if ([currentKey isEqualToString:URL_KEY]) {
            NSString *currentValue = (NSString *)[datum objectForKey:@"value"];
            
            // update the web page to point to the correct page
            [self setWebViewAddress:currentValue];
        }
    }
}

- (void)topic:(ACBTopic *)topic didDeleteWithMessage:(NSString *)message
{
    NSLog(@":::didDeleteWithMessage");
    [self topicDidDelete:topic];
}

- (void)topic:(ACBTopic *)topic didSubmitWithKey:(NSString *)key value:(NSString *)value version:(int)version
{
    NSLog(@"didSubmitWithKey");
    if ([key isEqualToString:URL_KEY]) {
        [self setWebViewAddress:value];
    }
}

- (void)topic:(ACBTopic *)topic didDeleteDataSuccessfullyWithKey:(NSString *)key version:(int)version
{
    NSLog(@"didDeleteDataSuccessfullyWithKey");
    if ([key isEqualToString:URL_KEY]) {
        [self setWebViewAddress:@"about:blank"];
    }
}

- (void)topic:(ACBTopic *)topic didSendMessageSuccessfullyWithMessage:(NSString *)message
{
    NSLog(@"didSendMessageSuccessfullyWithMessage");

    [self logMessage:message];
}

- (void)topic:(ACBTopic *)topic didNotConnectWithMessage:(NSString *)message
{
    NSLog(@"didNotConnectWithMessage");
}


- (void)topic:(ACBTopic *)topic didNotDeleteWithMessage:(NSString *)message
{
    NSLog(@"didNotDeleteWithMessage");
}


- (void)topic:(ACBTopic *)topic didNotSubmitWithKey:(NSString *)key value:(NSString *)value message:(NSString *)message
{
    NSLog(@"didNotSubmitWithKey");
}

- (void)topic:(ACBTopic *)topic didNotDeleteDataWithKey:(NSString *)key message:(NSString *)message
{
    NSLog(@"didNotDeleteDataWithKey");
}

- (void)topic:(ACBTopic *)topic didNotSendMessage:(NSString *)message message:(NSString *)errorMessage
{
    NSLog(@"didNotSendMessageWithError");
}


- (void)topicDidDelete:(ACBTopic *)topic
{
    NSLog(@"topicDidDelete");
    [self setWebViewAddress:@"about:blank"];
}


- (void)topic:(ACBTopic *)topic didUpdateWithKey:(NSString *)key value:(NSString *)value version:(int)version deleted:(BOOL)deleted
{
    NSLog(@"didUpdateWithKey");
    if ([key isEqualToString:URL_KEY]) {
        [self setWebViewAddress:value];
    }
}

- (void)topic:(ACBTopic *)topic didReceiveMessage:(NSString *)message
{
    NSLog(@"didReceiveMessage");
    [self logMessage:message];
}

@end
