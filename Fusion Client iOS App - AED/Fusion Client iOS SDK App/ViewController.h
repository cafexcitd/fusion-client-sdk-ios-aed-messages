//
//  ViewController.h
//  Fusion Client iOS SDK App
//
//

#import <UIKit/UIKit.h>
#import <ACBClientSDK/ACBUC.h>

@interface ViewController : UIViewController <ACBTopicDelegate>

// UI methods
-(IBAction)updateURI:(id)sender;
-(IBAction)sendMessage:(id)sender;

// UI components
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UITextField *uri;
@property (weak, nonatomic) IBOutlet UITextField *message;
@property (weak, nonatomic) IBOutlet UITextView *log;

@property (strong, nonatomic) ACBTopic *topic;

@end
